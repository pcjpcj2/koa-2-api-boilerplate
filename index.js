const app = require('./server')
const config = require('./config')

app.listen(config.PORT, () => {
  require('./models') // Load Model Schema
  console.log(`Server starting ${config.PORT}`)
})
