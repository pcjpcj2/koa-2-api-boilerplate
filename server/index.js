const Koa = require('koa')
const koaBody = require('koa-body')
const cors = require('@koa/cors')
const Router = require('koa-router');
const passport = require('koa-passport')

const routes = require('../routes')

const app = new Koa()
const router = new Router()

/**
 * See [bodyparser documents](https://github.com/koajs/bodyparser)
 */
app.use(koaBody())

/**
 * See [cors documents](https://github.com/koajs/cors)
 */
app.use(cors())

/**
 * See [passport documents](https://github.com/rkusa/koa-passport)
 */
app.use(passport.initialize())

/**
 * See [Koa Router documents](https://github.com/alexmingoia/koa-router)
 */
routes.forEach(route => app.use(route.routes()))

app
  .use(router.routes())
  .use(router.allowedMethods())

module.exports = app
