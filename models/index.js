const readdirpSync = require('fs-readdirp').readdirpSync

const capitalize = require('../utils/capitalize')
const isNotModuleFile = require('../utils/is-not-module-file')
const getBaseName = require('../utils/get-basename')
const arrayToObject = require('../utils/array-to-object')
const mongoose = require('../db')

const getFilePathObjectFromPath = (filePath, stats) => {
  const isNotModule = isNotModuleFile(filePath, stats)

  if (isNotModule) return false

  const name = capitalize(getBaseName(filePath))
  return { name, filePath }
}
const createMongooseModel = ({name, filePath: schemaPath}) => mongoose.model(name, require(schemaPath))
const redueModelArrayToObject = (obj, item) => arrayToObject(obj, item, 'modelName')

module.exports = readdirpSync(__dirname, getFilePathObjectFromPath)
.map(createMongooseModel)
.reduce(redueModelArrayToObject, {})
